-----------------------------------
KENAHOOT APP -- THE GOOD, THE BAD AND THE UGLY
-----------------------------------

## THE CLIENT
  ```client/index.html```
  This is where the magic happens.
  
### Standard HTML  
  Make sure bootstrap (and bootstrap-theme) css and your custom stylesheet ```css/style.css``` are linked (and loaded in that order)
  Make sure jQuery, bootstrap.js, angular.js and your custom javascript file ```js/script.js``` are linked (and loaded in that order)

### Angular components
  Append ```ng-app``` within the html tag
  
  At the earliest ```<div>``` where ... are expected to be dynamically loaded, append ```ng-controller``` You may want to truncate the controller name with ```ctrl```. In this case, it is the container holding the form panel.

  The next panel is the form panel, which holds the question, the options, the comment and the submit button panels. Append ```ng-submit``` to the form and call the submit function in the controller. (At his point you may wish to pause and start coding for the controller)

  To dynamically load data from the controller, insert ```{{ctrl.questions.name}}``` to the HTML where field is expected. 

  The same goes for the feedback message for checking the answer beside or below the submit button ```{{ctrl.finalanswer.message}}``` and all options for the radio buttons. ```{{ctrl.quiz.op1.name}}```

  All radio buttons have a value ```{{ctrl.quiz.op1.value}}``` populated from the controller with the use of the ```ng-model```. (Why does it point to ctrl.finalanswer.value?)

## SERVER
  Always ```"use strict"``` because you are kinky and like to be hit by syntax errors (Allows syntax errors to be caught early)

  Load relevant node modules such as express and body parser. ```var express = require("express");```

  ```var app = express();``` to simplify express calls. Extend some bodyParser functions for use in express calls with ```app.use```

  Specify NODE_PORT with ```const NODE_PORT = process.env.NODE_PORT || 3000;``` 

  Specify endpoint (directory) for express ```express().use(express.static(__dirname + "/../client/"));```

  Hardcode sample data in a data structure that is easy to call from index.html (An array with key-value and embedded key-value pairs)

  Use ```app.get``` for randomizing the questions propagated to index.html

  To create a response to a submitted answer use ```app.post```, create variable for body of request ```var quiz = req.body```. You can check the answers by creating an if-else statement, assigning a boolean state ```quiz.isCorrect = true;``` or otherwise. This is a promise which should return a response within a certain period. ```res.status(200).json(quiz);```

  You might want to use an ```app.use``` to deliver an error message to the browser when all else before this does not get triggered. ```res.send("<h1>Error! Page not found " + "</h1>");```

  Use ```app.listen``` to listen to any requests from the browser. You may want to print a statement in the console to show that this is being activated

## CONTROLLER

  Create an IIFE function and ```"use strict"``` because you like to be disciplined

  This does something like load $http and RegAppCtrl into itself (RegAppCtrl). ```angular.module("RegApp", []).controller("RegAppCtrl", ["$http", RegAppCtrl]);```

  Next create a function in the IIFE called RegAppCtrl taking in $http as an argument.

  ```var self = this;```

  Initialize some variables to be referenced between View (index.html) and Model (server/app.js) eg. ```self.quiz``` and ```self.finalanswer```

  Initialize the form with a function using ```$http.get("/popquizes")``` to get data from the specified endpoint on the server, also calling the methods ```.then(function(result))``` and ```.catch(function(e))``` to show results or error depending on the outcome of the get request

  Create the submit quiz function which will use the ```$http.post``` function, taking in 2 arguments (endpoint, finalanswer), also calling the methods then and catch depending on outcome of the request. For a successful request, run an if-else statement on the result, returning a correct message if correct, and vice versa.