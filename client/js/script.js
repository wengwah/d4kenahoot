

(function() {
  "use strict";
  var app = angular.module("KenahootApp", []);

  // app.controller(param1, param2)
  // param1 searches script.js for the function by name (therefore string)
  // param2 injects the following functions into param1
  app.controller("KenahootCtrl", ["$http", KenahootCtrl]);

  function KenahootCtrl($http) {
    var self = this; // vm

    self.quiz = {

    };

    self.finalanswer = {
      id: 0,
      value: "",
      comments: "",
      message: ""

    };

    self.initForm = function() {
      console.log("initForm");
      $http.get("/popquizes")
      .then(function(result) {
        console.log(">>>> " + result);
        console.log(">>>> " + JSON.stringify(result.data));
        
        self.quiz = result.data; // why .data for results?
        console.log(self.quiz.op1.name);
        console.log(self.quiz.op1.value);
        
      })
      .catch(function(e) {
        console.log(e);
      });
    }

    self.initForm();

    self.submitQuiz = function() {
      console.log("Quiz submitted!");
      self.finalanswer.id = self.quiz.id; // why do this?

      $http.post("/submit-quiz", self.finalanswer)
        .then(function(result) {
          console.log("Result is " + result);
          if(result.data.isCorrect) { // why is there no comparison here?
            self.finalanswer.message = "Correct!";
          } else {
            self.finalanswer.message = "Wrong!"
          }
        })
        .catch(function(e) {
          console.log(e);
        });

    }; // submitQuiz function bracket

  } // kahootCtrl function bracket

}) ();