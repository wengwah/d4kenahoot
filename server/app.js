
"use strict"; // this allows us to catch syntax errors early
console.log("Starting...");

// Loading node server libraries
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3000;
app.use(express.static(__dirname + "/../client/"));

// Hardcode question bank here

var questions = [
  {
    id: 0,
    question: "What does MEAN stack stands for?",        
    op1: { 
      name: "MongoDB, Express.js, AngularJS (or Angular), and Node.js",
      value: 1
    },
    op2: { 
      name: "MongoDB, Express.js, AngularJS (or Angular), and Java",
      value: 2
    },
    op3: {
      name: "MongoDB, Express.js, AngularJS (or Angular), and .Net",
      value: 3
    },
    op4: {
      name: "MongoDB, Express.js, AngularJS (or Angular), and Python",
      value: 4
    },
    answer: 1
  },

  {
    id: 1,
    question: "Which git command use to pull down other people's project on github/bitbucket?",
    op1: {
      name: "git clone",
      value: 1
    },
    op2: {
      name: "git copy",
      value: 2
    },
    op3: {
      name: "git merge",
      value: 3
    },
    op4: {
      name: "git pull refresh",
      value: 4
    }, 
    answer: 1
  },

  {
    id: 2,
    question: "How do we print things out on terminal in JS?",        
    op1: {
      name: "System.out.println",
      value: 1
    },
    op2: {
      name: "print",
      value: 2
    },
    op3: {
      name: "printf",
      value: 3
    },
    op4: {
      name: "console.log",
      value: 4
    },
    answer: 4
  }

];

    // var question1 = {
    //   id: 0,
    //   question: "What does MEAN stack stands for?",        
    //   op1: { 
    //     name: "MongoDB, Express.js, AngularJS (or Angular), and Node.js",
    //     value: 1
    //   },
    //   op2: { 
    //     name: "MongoDB, Express.js, AngularJS (or Angular), and Java",
    //     value: 2
    //   },
    //   op3: {
    //     name: "MongoDB, Express.js, AngularJS (or Angular), and .Net",
    //     value: 3
    //   },
    //   op4: {
    //     name: "MongoDB, Express.js, AngularJS (or Angular), and Python",
    //     value: 4
    //   },
    //   answer: 1
    // };
    
    // var question2 = {
    //   id: 1,
    //   question: "Which git command use to pull down other people's project on github/bitbucket?",
    //   op1: {
    //     name: "git clone",
    //     value: 1
    //   },
    //   op2: {
    //     name: "git copy",
    //     value: 2
    //   },
    //   op3: {
    //     name: "git merge",
    //     value: 3
    //   },
    //   op4: {
    //     name: "git pull refresh",
    //     value: 4
    //   }, 
    //   answer: 1
    // };
    
    // var question3 = {
    //   id: 2,
    //   question: "How do we print things out on terminal in JS?",        
    //   op1: {
    //     name: "System.out.println",
    //     value: 1
    //   },
    //   op2: {
    //     name: "print",
    //     value: 2
    //   },
    //   op3: {
    //     name: "printf",
    //     value: 3
    //   },
    //   op4: {
    //     name: "console.log",
    //     value: 4
    //   },
    //   answer: 4
    // };

// 3 questions only!    
    // var question4 = {
    //     question: "What sort of database is MongoDB?",
    //     options: {
    //       op1: "NoSQL Database (Document based)",
    //       op2: "GotSQL",
    //       op3: "AlotSQL",
    //       op4: "RDBMS"
    //     }, 
    //     answer: 1
    // };
    
    // var question5 = {
    //     question: "How can we check-in our codes to bitbucket?",
    //     options: {
    //       op1: 'git add . , git commit -m "test", git push origin master',
    //       op2: 'git add . , git commit -m "test" git push over',
    //       op3: 'git add . , git commit -m "test" , git pull',
    //       op4: 'git add . , git commit -m "test" git push branch master'
    //     }, 
    //     answer: 1
    // };
    
    // var question6 = {
    //     question: "What is a Node JS?",
    //     options: {
    //       op1: "Virtual Machine (JS Engine)",
    //       op2: "JS Stack",
    //       op3: "Operating System",
    //       op4: "Database"
    //     }, 
    //     answer: 1
    // };
    
    // var question7 = {
    //     question: "How to check data type in JS?",
    //     options: {
    //       op1: "typeof",
    //       op2: "instance",
    //       op3: "whichinstance",
    //       op4: "instance_of"
    //     }, 
    //     answer: 1
    // };
    
    // var question8 = {
    //     question: "How to create a new branch?",
    //     options: {
    //       op1: "git checkout -b feature_X",
    //       op2: "git pull -b feature_X",
    //       op3: "git checkin -b feature_X",
    //       op4: "git merge -b feature_X"
    //     }, 
    //     answer: 1
    // };
    
    // var question9 = {
    //     question: "What syntax iterate elements?",
    //     options: {
    //       op1: "ForEach",
    //       op2: "forNext",
    //       op3: "forEach",
    //       op4: "iterator"
    //     }, 
    //     answer: 3
    // };
    
    // var question10 = {
    //     question: "What is a callback function?",
    //     options: [{
    //       op1: "A callback function, also known as a higher-order function",
    //       op2: "is a hell function",
    //       op3: "anonymous function",
    //       op4: "Throwable function"
    //     }], 
    //     answer: 1
    // };

    // var questions = [question1, question2, question3];


app.get("/popquizes", function(req, res) {
  // var i = Math.floor(Math.random() * this.questions.length);
  // console.log(i);
  // res.json(questions[i]);

  var x = Math.random() * (3 - 1) + 1;
  var y = Math.floor(x);
  console.log(">>>> XXXX  " + y);
  console.log(questions[y]);
  res.status(200).json(questions[y]);

});

app.post("/submit-quiz", function(req, res) {
  console.log("Received user object " + req.body);
  console.log("Received user object " + JSON.stringify(req.body));
  var quiz = req.body;
  var checking = questions[quiz.id];
  if (checking.answer == parseInt(quiz.value)){
      console.log("CORRECT !");
      quiz.isCorrect = true;
  }else{
      console.log("INCORRECT !");
      quiz.isCorrect = false;
  }
  res.status(200).json(quiz);
});

// app.post("/users", function(req, res) {
//   // console.log(req);
//   console.log("Received user object " + req.body);
//   console.log("Received user object " + JSON.stringify(req.body));

//   res.status(200).json(user);
// });

app.use(function(req, res) {
    console.log("Accessing...");
    res.send("<h1>Error! Page not found " + "</h1>"); // why does this get triggered?
});

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);

// app.use, app.post, res.send, res.status are all express functions
// refer to documentations in express
});